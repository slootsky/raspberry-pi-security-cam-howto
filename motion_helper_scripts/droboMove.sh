#!/bin/bash 
# reminder : add -x after bash to enable debuggin

savepath=/home/pi/Pictures/motion-output

function create_path(){
        ## echo Create Path $1
        if [[ -n "$1" ]] && [[ "$1" != "/" ]] ;
        then
                create_path `dirname $1`
        fi
        mkdir $1 2> /dev/null
}


if [[ -n "$1" ]] ;
then
        if grep drobo /proc/mounts > /dev/null ;
        then
                if [[ $1 != /* ]] ;
                then
                        slash=/
                else
                        slash=""
                fi
                srcDir=`dirname $1`
                destination=/mnt/drobo/`hostname`${slash}${srcDir#$savepath}/
                create_path $destination

                echo move $1 to $destination >> /home/pi/droboMove.log
                mv $1 $destination 2>> /home/pi/droboMove.log 
        else
                echo /mnt/drobo not yet mounted >> /home/pi/droboMove.log
        fi
fi